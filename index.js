const path = require('path');
const fs = require('fs');
const Excel = require('exceljs');
const nodemailer = require('nodemailer');

const getBytes = (param) => {
    let unit = param.slice(-1);
    let number = param.slice(0, -1);
    let result;
    let kb = 1024;
    let mb = 1024 * kb;
    let gb = 1024 * mb;
    if (unit === 'B') {
        result = +number;
    }
    if (unit === 'K') {
        result = +number * kb;
    }
    if (unit === 'M') {
        result = +number * mb;
    }
    if (unit === 'G') {
        result = +number * gb;
    }

    return result;
};

const getParam = regexp => {
    const [node, executable, ...params] = process.argv;
    return params.map(i => regexp.exec(i)).find(i => i);
};

const EMAIL_TO_REGEX = /--EMAIL-TO=(.*)/i;
const DIR_REGEX = /--DIR=(.*)/i;
const TYPE_REGEX = /--TYPE=(.*)/i;
const PATTERN_REGEX = /--PATTERN=(.*)/i;
const MIN_SIZE_REGEX = /--MIN-SIZE=(.*)/i;
const MAX_SIZE_REGEX = /--MAX-SIZE=(.*)/i;

const EMAIL = getParam(EMAIL_TO_REGEX);
const DIR = getParam(DIR_REGEX);
const TYPE = getParam(TYPE_REGEX);
const PATTERN = getParam(PATTERN_REGEX);
const MIN_SIZE = getParam(MIN_SIZE_REGEX);
const MAX_SIZE = getParam(MAX_SIZE_REGEX);

if (!DIR || !EMAIL) {
    throw Error('Argument Error');
}

const param = {};
param.email = EMAIL[1];
param.dir = DIR[1];
if (TYPE) {
    param.type = TYPE[1];
}
if (PATTERN) {
    param.pattern = new RegExp(PATTERN[1]);
}
if (MIN_SIZE) {
    param.minSize = getBytes(MIN_SIZE[1]);
}
if (MAX_SIZE) {
    param.maxSize = getBytes(MAX_SIZE[1]);
}

Promise.all([
    getSmtpConfig('smtp.properties'),
    getFiles(param.dir, param)

]).then(([smtpConfig, items]) => {
    let workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet('Results');
    worksheet.getColumn(1).width = 100;
    worksheet.addRows(items.map((item) => [item]));
    workbook.xlsx.writeFile('results.xlsx');

    sendEmail(smtpConfig, 'results.xlsx', param.email);

}).then(() => {
    console.log('Email Successfully Sent');
}).catch((err) => {
    throw err;
});


function getSmtpConfig(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, result) => {
            if (err) reject(err);
            resolve(JSON.parse(result));
        })
    })
}

function getFiles(dir, param) {
    let results = [];
    return new Promise((resolve, reject) => {
        fs.readdir(dir, (err, list) => {
            if (err) return reject(err);
            let pending = list.length;
            if (!pending) return resolve(results);
            list.forEach(function (item) {
                let file = path.resolve(dir, item);
                fs.stat(file, (err, stat) => {
                    if (stat && stat.isDirectory()) {
                        if (!param.type || param.type === 'D') {
                            if (!param.pattern || param.pattern.test(item)) {
                                results.push(file);
                            }
                        }
                        getFiles(file, param).then((res) => {
                            results.push(...res);
                            if (!--pending) resolve(results);
                        });
                    } else {
                        if (!param.type || param.type === 'F') {
                            if ((!param.pattern || param.pattern.test(item))
                                && (!param.maxSize || stat.size <= param.maxSize)
                                && (!param.minSize || stat.size >= param.minSize)) {
                                results.push(file);
                            }
                        }
                        if (!--pending) resolve(results);
                    }
                });
            });
        });
    })
}

function sendEmail(smtpConfig, file, email) {
    let transporter = nodemailer.createTransport(smtpConfig);
    return transporter.sendMail({
        from: smtpConfig.auth.user,
        to: email,
        subject: "Result of your search",
        attachments: [
            {
                path: file
            }
        ]
    });
}
